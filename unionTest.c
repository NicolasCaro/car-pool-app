#include <stdio.h>
#include <string.h>

union MyStuff {
    int hehe;
    char first[15];
};

int main() {
    union MyStuff things;
    
    things.hehe = 10;
    
    printf("I giggled %d times\n", things.hehe);
    
    strcpy(things.first, "loves me");
    
    printf("But she %s.\n", things.first);

    return 0;

}
