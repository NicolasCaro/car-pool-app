#include <stdio.h>
#include <string.h>

struct heroes {
    char type[10];
    char power[50];
    int age;
    char xd[40];

};
void printhero(struct heroes heroes);
int main()
{
    struct heroes thehero;
    struct heroes myhero;

    strcpy(thehero.type, "buffboy");
    strcpy(thehero.power, "superkicking");
    thehero.age = 32;
    strcpy(thehero.xd, "Human");

    strcpy(myhero.type, "coolguy");
    strcpy(myhero.power, "mindControl");
    myhero.age = 26;
    strcpy(myhero.xd, "Elven");

    printhero(thehero);
    printf("\n");
    printhero(myhero);

    return(0);
}

void printhero( struct heroes heroes){
    printf("Hero Type : %s\n", heroes.type);
    printf("Hero Power : %s\n", heroes.power);
    printf("Hero Race : %s\n", heroes.xd);
    printf("Hero Age : %d\n", heroes.age);
}
